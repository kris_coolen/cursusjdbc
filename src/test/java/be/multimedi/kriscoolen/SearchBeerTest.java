package be.multimedi.kriscoolen;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import java.sql.*;

import java.sql.DriverManager;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SearchBeerTest {
    Connection con;
    Statement stmt;
    @BeforeAll
    public void openConnection() throws SQLException {
        con = DriverManager.getConnection("jdbc:mariadb://javaeegenk.be/javaeegenkDB1","javaeegenk","java%%g%nk2019");
        stmt = con.createStatement();
    }
    @AfterAll
    public void closeConnection() throws SQLException{
        con.close();
    }

    @Test
    public void searchBeerById() throws SQLException{
        ResultSet rs = stmt.executeQuery("SELECT * FROM Beers WHERE Beers.Id=168");
        rs.next();
        assertEquals(168,rs.getInt(1));
        assertEquals("Bokkereyer",rs.getString(2));
        assertEquals(103,rs.getInt(3));
        assertEquals(6,rs.getInt(4));
        assertEquals(2.55,rs.getDouble(5));
        assertEquals(100,rs.getInt(6));
        assertEquals(5,rs.getDouble(7));

    }


}
