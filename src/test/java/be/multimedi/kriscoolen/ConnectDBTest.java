package be.multimedi.kriscoolen;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;
import java.sql.*;

import java.sql.DriverManager;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConnectDBTest {

    Connection con;

    @BeforeAll
    public void openConnection() throws SQLException {
        con = DriverManager.getConnection("jdbc:mariadb://javaeegenk.be/javaeegenkDB1","javaeegenk","java%%g%nk2019");

    }

    @AfterAll
    public void closeConnection() throws SQLException{
        con.close();
    }

    @Test
    public void testConnection() throws SQLException {
        assertTrue(con.isValid(0));
        con.close();
        assertTrue(con.isClosed());
   }
}