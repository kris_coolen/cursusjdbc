package be.multimedi.kriscoolen;

import java.sql.*;
public class SearchBeer
{
    public static void main( String[] args )
    {
        //String sql = "SELECT Name, Alcohol, Price FROM Beers";
        //String sql = "SELECT Name, Alcohol, Price FROM Beers ORDER BY Alcohol";
        //String sql = "SELECT NAME, Alcohol, Price FROM Beers WHERE Alcohol=8.0";
        String sql = "SELECT Beers.Name, Beers.Alcohol, Beers.Price, Brewers.Name, Brewers.City, Categories.Category FROM Beers JOIN Brewers ON Beers.BrewerId=Brewers.Id JOIN Categories ON Beers.CategoryId=Categories.Id;";

        try(Connection con = DriverManager.getConnection(
                "jdbc:mariadb://javaeegenk.be/javaeegenkDB1","javaeegenk","java%%g%nk2019");
            Statement stmt = con.createStatement();ResultSet rs = stmt.executeQuery(sql))
        {
            System.out.println("                              BeerName                            alcohol         price     Brewer                   City            Category");
            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------");
            while(rs.next()){
               String beerName = rs.getString(1) ;
               double alcohol = rs.getDouble(2);
               double price = rs.getDouble(3);
               String brewerName = rs.getString(4);
               String city = rs.getString(5);
               String cat = rs.getString(6);


               //if(Double.compare(alcohol,8.0)==0)
                //System.out.format("%-62s%3s%15s%n", beerName, alcohol, price);
                System.out.format("%-65s %5.1f %15.2f    %-20s %-20s %-15s %n", beerName, alcohol, price,brewerName,city,cat);
            }
        }
        catch (Exception ex){
            System.out.println("Oops, something went wrong!");
            ex.printStackTrace(System.err);
        }
    }
}